package com.openclassrooms.tourguide;


import com.openclassrooms.tourguide.user.User;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUser {


    @Test
    public void testGetLastVisitedLocation() throws ParseException {

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<VisitedLocation> visitedLocations =  new CopyOnWriteArrayList<>();

        Location firstLocation = new Location(1, 1);
        Location secondLocation = new Location(2, 2);
        Location thirdLocation = new Location(3, 3);

        String firstDateString = "2023-06-23 14:30:00";
        String secondDateString = "2023-06-24 14:30:00";
        String thirdDateString = "2023-06-25 14:30:00";

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date firstDate = formatter.parse(firstDateString);
        Date secondDate = formatter.parse(secondDateString);
        Date thirdDate = formatter.parse(thirdDateString);

        VisitedLocation firstVisitedLocation = new VisitedLocation(user.getUserId(), firstLocation, firstDate);
        VisitedLocation secondVisitedLocation = new VisitedLocation(user.getUserId(), secondLocation, secondDate);
        VisitedLocation thirdVisitedLocation = new VisitedLocation(user.getUserId(), thirdLocation, thirdDate);

        visitedLocations.add(firstVisitedLocation);
        visitedLocations.add(secondVisitedLocation);
        visitedLocations.add(thirdVisitedLocation);

        user.setVisitedLocations(visitedLocations);

        VisitedLocation lastVisitedLocation = user.getLastVisitedLocation();

        assertEquals(thirdVisitedLocation, lastVisitedLocation);
    }

    @Test
    public void testClearVisitedLocations() throws ParseException {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<VisitedLocation> visitedLocations =  new CopyOnWriteArrayList<>();

        Location firstLocation = new Location(1, 1);
        Location secondLocation = new Location(2, 2);
        Location thirdLocation = new Location(3, 3);

        String firstDateString = "2023-06-23 14:30:00";
        String secondDateString = "2023-06-24 14:30:00";
        String thirdDateString = "2023-06-25 14:30:00";

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date firstDate = formatter.parse(firstDateString);
        Date secondDate = formatter.parse(secondDateString);
        Date thirdDate = formatter.parse(thirdDateString);

        VisitedLocation firstVisitedLocation = new VisitedLocation(user.getUserId(), firstLocation, firstDate);
        VisitedLocation secondVisitedLocation = new VisitedLocation(user.getUserId(), secondLocation, secondDate);
        VisitedLocation thirdVisitedLocation = new VisitedLocation(user.getUserId(), thirdLocation, thirdDate);

        visitedLocations.add(firstVisitedLocation);
        visitedLocations.add(secondVisitedLocation);
        visitedLocations.add(thirdVisitedLocation);

        user.setVisitedLocations(visitedLocations);
        user.clearVisitedLocations();

        List<VisitedLocation> result = user.getVisitedLocations();

        assertEquals(0, result.size());
    }

}
