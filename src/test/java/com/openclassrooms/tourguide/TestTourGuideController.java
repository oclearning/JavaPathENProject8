package com.openclassrooms.tourguide;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.openclassrooms.tourguide.user.User;
import com.openclassrooms.tourguide.service.TourGuideService;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TourGuideController.class)
public class TestTourGuideController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TourGuideService tourGuideService;

    private User testUser;
    private VisitedLocation testVisitedLocation;
    private JSONObject testAttractions;

    @BeforeEach
    public void setUp() throws ParseException, JSONException, ExecutionException, InterruptedException {
        testUser = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Location location = new Location(1d, 2d);
        String dateString = "2023-06-23 14:30:00";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = formatter.parse(dateString);
        testVisitedLocation = new VisitedLocation(testUser.getUserId(), location, date);

        testAttractions = new JSONObject();
        testAttractions.put("attractionName", "Statue of Liberty");
        testAttractions.put("location", new JSONObject().put("latitude", 40.6892).put("longitude", -74.0445));

        when(tourGuideService.getUser("jon")).thenReturn(testUser);
        when(tourGuideService.getUserLocation(testUser)).thenReturn(testVisitedLocation);
        when(tourGuideService.getNearByAttractions(any(VisitedLocation.class))).thenReturn(testAttractions);
    }

    @Test
    public void testIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string("Greetings from TourGuide!"));
    }

    @Test
    public void testGetLocation() throws Exception {
        mockMvc.perform(get("/getLocation")
                        .param("userName", "jon"))
                .andExpect(status().isOk());

        verify(tourGuideService, times(1)).getUserLocation(testUser);
    }

    @Test
    public void testGetNearbyAttractions() throws Exception {
        mockMvc.perform(get("/getNearbyAttractions")
                        .param("userName", "jon"))
                .andExpect(status().isOk());

        verify(tourGuideService, times(1)).getNearByAttractions(testVisitedLocation);
    }

    @Test
    public void testGetRewards() throws Exception {
        mockMvc.perform(get("/getRewards")
                        .param("userName", "jon"))
                .andExpect(status().isOk());

        verify(tourGuideService, times(1)).getUserRewards(testUser);
    }

    @Test
    public void testGetTripDeals() throws Exception {

        mockMvc.perform(get("/getTripDeals")
                        .param("userName", "jon"))
                .andExpect(status().isOk());

        verify(tourGuideService, times(1)).getTripDeals(testUser);
    }
}