package com.openclassrooms.tourguide;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import com.openclassrooms.tourguide.user.UserReward;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import com.openclassrooms.tourguide.helper.InternalTestHelper;
import com.openclassrooms.tourguide.service.RewardsService;
import com.openclassrooms.tourguide.service.TourGuideService;
import com.openclassrooms.tourguide.user.User;

import static org.junit.jupiter.api.Assertions.*;

public class TestPerformance {

    private List<RewardsService> rewardsServices = new ArrayList<>();
    private List<TourGuideService> tourGuideServices = new ArrayList<>();

    @AfterEach
    public void tearDown() {
        rewardsServices.forEach(RewardsService::shutdown);
        tourGuideServices.forEach(TourGuideService::shutdown);
    }

    private RewardsService createRewardsService() {
        RewardsService rewardsService = new RewardsService(new GpsUtil(), new RewardCentral());
        rewardsServices.add(rewardsService);
        return rewardsService;
    }

    private TourGuideService createTourGuideService(RewardsService rewardsService) {
        TourGuideService tourGuideService = new TourGuideService(new GpsUtil(), rewardsService);
        tourGuideServices.add(tourGuideService);
        return tourGuideService;
    }


    @Test
    public void highVolumeTrackLocationAsync() throws InterruptedException, ExecutionException {

        RewardsService rewardsService = createRewardsService();

        InternalTestHelper.setInternalUserNumber(100000);
        TourGuideService tourGuideService = createTourGuideService(rewardsService);

        List<User> allUsers = tourGuideService.getAllUsers();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        List<CompletableFuture<VisitedLocation>> locationFutures = allUsers.stream()
                .map(tourGuideService::trackUserLocationAsync)
                .toList();

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(locationFutures.toArray(new CompletableFuture[0]));
        allFutures.get();
        allFutures.join();

        stopWatch.stop();
        tourGuideService.tracker.stopTracking();

        System.out.println("highVolumeTrackLocationAsync: Time Elapsed: "
                + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));

        assertEquals(100000, allUsers.size());

        for (User user : allUsers) {
            assertFalse(user.getVisitedLocations().isEmpty());
        }
    }

    @Test
    public void testTrackUserLocationConsistency() throws ExecutionException, InterruptedException {

        RewardsService rewardsService = createRewardsService();

        InternalTestHelper.setInternalUserNumber(100);
        TourGuideService tourGuideService = createTourGuideService(rewardsService);

        // Create two lists to test the output of both Sync and Async methods, the lists contain the same Users
        List<User> allUsers = tourGuideService.getAllUsers();
        List<User> allUsersAsync = new ArrayList<>(allUsers);


        // Track users synchronously
        StopWatch stopWatchSync = new StopWatch();
        stopWatchSync.start();
        for (User user : allUsers) {
            tourGuideService.trackUserLocation(user);
        }
        stopWatchSync.stop();
        tourGuideService.tracker.stopTracking();

        // Track users asynchronously
        StopWatch stopWatchAsync = new StopWatch();
        stopWatchAsync.start();
        List<CompletableFuture<VisitedLocation>> locationFutures = allUsersAsync.stream()
                .map(tourGuideService::trackUserLocationAsync)
                .toList();

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(locationFutures.toArray(new CompletableFuture[0]));
        allFutures.get();
        allFutures.join();
        stopWatchAsync.stop();
        tourGuideService.tracker.stopTracking();


        //Assert that the asynchronous method is faster
        assertTrue((stopWatchSync.getTime()) > (stopWatchAsync.getTime()));
        System.out.println("Sync time: " + stopWatchSync.getTime() + " ms");
        System.out.println("Async time: " + stopWatchAsync.getTime() + " ms");

        //Assert that the synchronous and asynchronous methods yield the same result

        assertEquals(allUsers.size(), allUsersAsync.size());

        // Assert that the number of visited locations are the same
        IntStream.range(0, allUsers.size()).forEach(i -> {
            User userSync = allUsers.get(i);
            User userAsync = allUsersAsync.get(i);

            List<VisitedLocation> visitedLocationsSync = userSync.getVisitedLocations();
            List<VisitedLocation> visitedLocationsAsync = userAsync.getVisitedLocations();
            assertEquals(visitedLocationsSync.size(), visitedLocationsAsync.size());

            // Assert that the visited locations are the same
            IntStream.range(0, visitedLocationsSync.size()).forEach(j -> {
                assertEquals(visitedLocationsSync.get(j).location.latitude, visitedLocationsAsync.get(j).location.latitude);
                assertEquals(visitedLocationsSync.get(j).location.longitude, visitedLocationsAsync.get(j).location.longitude);
            });

            // Assert that the rewards are the same
            List<UserReward> userRewardsSync = userSync.getUserRewards();
            List<UserReward> userRewardsAsync = userAsync.getUserRewards();
            assertEquals(userRewardsSync.size(), userRewardsAsync.size());
            IntStream.range(0, userRewardsSync.size()).forEach(k -> {
                assertEquals(userRewardsSync.get(k).getRewardPoints(), userRewardsAsync.get(k).getRewardPoints());
            });
        });
    }

    @Test
    public void testTrackUserLocationAsyncRepeatedly() {
        RewardsService rewardsService = createRewardsService();
        TourGuideService tourGuideService = createTourGuideService(rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<CompletableFuture<VisitedLocation>> futures = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            CompletableFuture<VisitedLocation> future = tourGuideService.trackUserLocationAsync(user);
            futures.add(future);
        }

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allFutures.join();

        assertEquals(100, user.getVisitedLocations().size());
        user.getVisitedLocations().forEach(Assertions::assertNotNull);
    }



    @Test
    public void highVolumeGetRewardsAsync() throws ExecutionException, InterruptedException {
        GpsUtil gpsUtil = new GpsUtil();
        RewardsService rewardsService = createRewardsService();
        InternalTestHelper.setInternalUserNumber(100000);
        TourGuideService tourGuideService = createTourGuideService(rewardsService);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Attraction attraction = gpsUtil.getAttractions().get(0);
        List<User> allUsers = tourGuideService.getAllUsers();

        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        // Use async reward calculation

        List<CompletableFuture<Void>> futures = allUsers.stream()
                .map(rewardsService::calculateRewardsAsync)
                .toList();

        // Wait for all asynchronous calculations to complete
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allFutures.get();
        allFutures.join();

        stopWatch.stop();
        tourGuideService.tracker.stopTracking();

        // Assert that each user has rewards
        for (User user : allUsers) {
            assertFalse(user.getUserRewards().isEmpty());
        }

        // Assert that the call is faster than the goal of 20 minutes

        System.out.println("highVolumeGetRewardsAsync: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())
                + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }

    @Test
    public void testCalculateRewardsAsyncRepeatedly() throws ExecutionException, InterruptedException {
        RewardsService rewardsService = createRewardsService();
        GpsUtil gpsUtil = new GpsUtil();

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<Attraction> attractions =  gpsUtil.getAttractions();

        List<CompletableFuture<Void>> futures = new ArrayList<>();

        for(Attraction attraction : attractions) {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
            CompletableFuture<Void> future = rewardsService.calculateRewardsAsync(user);
            futures.add(future);
        }

        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allFutures.get();
        allFutures.join();

        assertFalse(user.getUserRewards().isEmpty());

        System.out.println("Number of rewards: " + user.getUserRewards().size());
    }


    @Test
    public void testCalculateRewardsConsistency() throws ExecutionException, InterruptedException {

        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsService rewardsServiceAsync = new RewardsService(gpsUtil, rewardCentral);
        RewardsService rewardsServiceSync = new RewardsService(gpsUtil, rewardCentral);

        InternalTestHelper.setInternalUserNumber(100);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsServiceAsync);

        // Get the list of all users
        List<User> allUsers = tourGuideService.getAllUsers();

        // Add visited locations for all users at a specific attraction
        Attraction attraction = gpsUtil.getAttractions().get(0);
        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        // Create second list to process asynchronously and be able to compare both lists
        List<User> allUsersAsync = new ArrayList<>(allUsers);

        // Calculate rewards for all users synchronously
        StopWatch stopWatchSync = new StopWatch();
        stopWatchSync.start();
        allUsers.forEach(rewardsServiceSync::calculateRewards);
        stopWatchSync.stop();

        // Calculate rewards for all users asynchronously
        StopWatch stopWatchAsync = new StopWatch();
        stopWatchAsync.start();
        allUsersAsync.forEach(rewardsServiceAsync::calculateRewardsAsync);

        List<CompletableFuture<Void>> futures = allUsersAsync.stream()
                .map(rewardsServiceAsync::calculateRewardsAsync)
                .toList();

        // Wait for all asynchronous calculations to complete
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allFutures.get();
        allFutures.join();
        stopWatchAsync.stop();

        tourGuideService.tracker.stopTracking();

        //Assert that the asynchronous method is faster
        assertTrue((stopWatchSync.getTime()) > (stopWatchAsync.getTime()));
        System.out.println("Sync time: " + stopWatchSync.getTime() + " ms");
        System.out.println("Async time: " + stopWatchAsync.getTime() + " ms");

        // Compare that sync and async method provide the same result
        for (int i = 0; i < allUsers.size(); i++) {
            List<UserReward> userRewardsSync = allUsers.get(i).getUserRewards();
            List<UserReward> userRewardsAsync = allUsersAsync.get(i).getUserRewards();

            assertIterableEquals(userRewardsSync, userRewardsAsync);
            assertEquals(userRewardsSync.size(), userRewardsAsync.size());
            assertTrue(userRewardsSync.containsAll(userRewardsAsync));
        }
    }

}
