package com.openclassrooms.tourguide;


import com.openclassrooms.tourguide.user.User;
import com.openclassrooms.tourguide.user.UserReward;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserReward {

    @Test
    public void testTwoArgsConstructor() throws ParseException {

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        Attraction attraction = new Attraction("Festival de la Palourde", "Saint-Malo", "Bretagne", 1d, 2d);

        Location location = new Location(1d, 2d);

        String dateString = "2023-06-23 14:30:00";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = formatter.parse(dateString);

        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, date);

        UserReward userReward = new UserReward(visitedLocation, attraction);

        assertEquals(visitedLocation, userReward.getVisitedLocation());
        assertEquals(attraction, userReward.getAttraction());
    }
}
